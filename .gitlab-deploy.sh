#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
    echo "Deploy project on server ${array[i]}"
    ssh ec2-user@${array[i]} "cd test_yu && git pull origin master && aws s3 cp gitlab_test.html s3://gitlab-cicd-test123"
done